# `RESHI`
RESHI is a simple automated application for smoothly clipping high intensity voxels in an MRI image.
For now it is focussed on T1-weighted images, where excessively high intensities in the scalp and other fatty tissues can negatively affect SSD-based nonlinear registration.
